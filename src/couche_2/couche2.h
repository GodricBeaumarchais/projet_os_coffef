/**
 * @file couche2.h
 * @brief couche regroupant les fonctions de la couche 2 du système s'occupant de la table d'inodes et du super_block.
 * @version 0.1
 * @date 2022-05-04
 * 
 * @copyright Copyright(c) 2022
 * 
 */

#ifndef _COUCHE2_H
#define _COUCHE2_H
#include "../global.h"
#include "../couche_1/couche1.h"

/**
 * @brief initialise le système à la couche 2
 * 
 * @param r_name nom du répertoire disque
 * @return int  valeur -1 si erreur, 0 sinon
 */
int init_disk_sos_2(char* r_name);

/**
 * @brief 
 * 
 * @return int 
 */
int init_super_block();


/**
 * @brief Permet l'écriture du superblock sur le disque depuis la variable global virtual_disk_sos
 * 
 * @return int  valeur -1 si erreur, 0 sinon
 */
int write_super_block();

/**
 * @brief Permet la lecture du super_block sur le disque sur la variable global virtual_disk_sos
 * 
 * @return 0 en cas de réussite, -1 en cas d'erreur hihi ^^
 */
int read_super_block();


/**
 * @brief trouve le premier octet libre pour les data sur le disque
 * il met à jour le super_block / doit être utilisé à chaque suppresion 
 * il écrit un fichier afin qu'il soit toujours à jour
 */
void first_free_bytes();

/**
 * @brief initialise la table d'inodes ainsi que le dd virtuel avant de le faire
 * 
 * @param [in] r_name nom du répertoire où stocker les données car il appelle la fonction init_disk_sos
 */
int init_inode_table();

/**
 * @brief permet de lire la table d'inodes sur le disque et la stocke dans la variable global virtual_disk_sos
 * 
 * @return 0 si tout se passe bien, -1 en cas d'erreur
 */
int read_inodes_table();

/**
 * @brief écrit la table d'inodes contenue dans la variable global virtual_disk_sos sur le disque à sa position
 * 
 * @return 0 si tout se passe bien, -1 en cas d'erreur
 */
int write_inodes_table();

/**
 * @brief lit une inode
 * 
 * @param id_inode inode à lire 
 * @return int 0 en cas de réussite, -1 en cas d'échec
 */
int read_inode(int id_inode);

/**
 * @brief écrit une inode
 * 
 * @param id_inode inode à écrire
 * @return int 0 en cas de réussite, -1 en cas d'échec
 */
int write_inode(int id_inode);

/** 
 * @brief supprime l'inode à la position i et compacte la table 
 * 
 */ 
void delete_inode(int i);


/**
 * @brief initialise l'inode placée en paramètres
 * 
 * @param [out] id_inode inode à initialiser
 */
void init_inode(int id_inode);

/**
 * @brief parcourt la table d'inodes et regarde la première qui n'a pas de fichier associé (taille_fich = 0)
 * 
 * @return retourne l'index de la première inode inutilisée dans la table d'inodes -1 si aucune inode n'est disponible
 */
int get_unused_inode();


/**
 * @brief met à jour une inode
 * 
 * @param id_inode position de l'inode dans la table
 * @param new_size nouvelle taille du fichier correnspondant
 */
void up_inode(int id_inode, int new_size);

/**
 * @brief set une inode grâce au nom du fichier et aux paramètres indiqués
 * 
 * @param [out] id_inode inode à modifier
 * @param [in] s_nom_fichier nom du fichier associé à l'inode
 * @param [in] i_taille_fich taille du fichier associé
 */
void set_inode(int id_inode,char* s_nom_fichier,int i_taille_fich);


/**
 * @brief ajoute une inode à la première position libre d'inode
 * 
 * @param [in] s_nom_fichier nom du fichier associé à l'inode
 * @param [in] i_taille_fich taille du fichier associé
 * @return int id de l'inode créer / -1 si aucune inode disponible
 */
int add_inode(char* s_nom_fichier,int i_taille_fich);

void cmd_dump_inode();

/**
 * @brief compacte la table d'inode sur la gauche, décale tous les éléments à gauche
 * 
 */
void compacte_inode_table();

/**
 * @brief concaténe deux chaines de caractères sur des tailles spécifiées
 * 
 * @param [out] buffer 
 * @param prem première chaîne
 * @param sec deuxième chaîne
 * @param prem_size taille première chaîne
 * @param sec_size taille deuxième chaîne
 */
void ustrcat(uchar * buffer, uchar * prem, uchar * sec, int prem_size, int sec_size);

/**
 * @brief éteint le système au niveau 2
 * 
 * @return int renvoie -1 en cas d'échec, 0 en cas de réussite
 */
int eteindre_2();

/**
 * @brief fonction permettant de récupérer la date
 * 
 * @return char* date récupérée
 */
char *timestamp();
#endif
