#include "couche2.h"

extern virtual_disk_t virtual_disk_sos;

/* ------------------------------------------ */
/*         GESTION DU SUPER BLOCK             */
/* ------------------------------------------ */


int init_disk_sos_2(char* r_name){
    //initialise le system
    int ret = init_disk_sos_1(r_name);
    if(ret == 1){

        if(init_super_block() == -1)return -1;
        if(init_inode_table() == -1)return -1;
        return 1;
    }
    else if(ret == 0){
        if(read_super_block() == -1)return -1;
        if(read_inodes_table() == -1)return -1;
        return 0;
    }
    else return -1;

}

int init_super_block(){
    super_block_t * s_b = &virtual_disk_sos.super_block;
    s_b->number_of_files = 0;
    s_b->number_of_users = 1;
    s_b->nb_blocks_used = DISK_SIZE;
    s_b->first_free_byte = (int)(DISK_SIZE)*4;
    return write_super_block();

}

/* Permet l'ecriture du superblock sur le disque */
int write_super_block(){
    super_block_t * s_b = &virtual_disk_sos.super_block;
    block_t s_b_data[SUPER_BLOCK_SIZE];
    block_t * buffer;
    buffer = uint_to_block(s_b->number_of_files);
    s_b_data[0] = *buffer;
    free(buffer);
    buffer = uint_to_block(s_b->number_of_users);
    s_b_data[1] = *buffer;
    free(buffer);
    buffer = uint_to_block(s_b->nb_blocks_used);
    s_b_data[2] = *buffer;
    free(buffer);
    buffer = uint_to_block(s_b->first_free_byte);
    s_b_data[3] = *buffer;
    free(buffer);
    return write_data(s_b_data,SUPER_BLOCK_SIZE,0);
    

}

/* ------------------------------------------ */

/* retourne un pointeur sur superblock lus */
int read_super_block(){
    super_block_t * s_b = &virtual_disk_sos.super_block;
    block_t s_b_data[SUPER_BLOCK_SIZE];
    int res = read_data( s_b_data, SUPER_BLOCK_SIZE, 0);
    s_b->number_of_files = block_to_uint(&s_b_data[0]);
    s_b->number_of_users = block_to_uint(&s_b_data[1]);
    s_b->nb_blocks_used = block_to_uint(&s_b_data[2]);
    s_b->first_free_byte = block_to_uint(&s_b_data[3]);
    return res;
}

/* ------------------------------------------ */


/* ------------------------------------------ */

// prend le premier octet des fichier et va au premier fichier libre 
// met a jour le super block avec le premier octet libre
void first_free_bytes(){
    
    bool place_prise[INODE_TABLE_SIZE];
    for(int i = 0; i < INODE_TABLE_SIZE; i++){
        place_prise[i] = false;
    }
    for(int i = 0; i < INODE_TABLE_SIZE; i++){
       

        if(virtual_disk_sos.inodes[i].size != 0 ){

            place_prise[(virtual_disk_sos.inodes[i].first_byte-(STORAGE_START*BLOCK_SIZE))/MAX_FILE_SIZE] = true;
    }
    } 
    int i = 0;
    while(place_prise[i])i++;
    virtual_disk_sos.super_block.first_free_byte = 4*(int)(DISK_SIZE)+MAX_FILE_SIZE*i;

    

}

/* ------------------------------------------ */
/*       FIN DE GESTION DU SUPER BLOCK        */
/* ------------------------------------------ */


/* ------------------------------------------ */
/*         GESTION DE LA TABLE INODE          */
/* ------------------------------------------ */


/* ------------------------------------------ */

/* permet de lire la tables d'inodes et de la remplir */
int read_inodes_table(){
    
    for(int i = 0; i < INODE_TABLE_SIZE; i++){
        if(read_inode(i) == -1)
            return -1;
    }
    return 0;

}

int read_inode(int id_inode){
    block_t inode_data[INODE_SIZE];
    int res = read_data(inode_data, INODE_SIZE,INODES_START+id_inode*27);
    claire_chan_reverse(virtual_disk_sos.inodes[id_inode].filename, inode_data, FILENAME_MAX_SIZE, 0);
    virtual_disk_sos.inodes[id_inode].size = block_to_uint(&inode_data[8]);
    virtual_disk_sos.inodes[id_inode].uid = block_to_uint(&inode_data[9]);
    virtual_disk_sos.inodes[id_inode].uright = block_to_uint(&inode_data[10]);
    virtual_disk_sos.inodes[id_inode].oright = block_to_uint(&inode_data[11]);
    uchar buffer[TIMESTAMP_SIZE*2];
    claire_chan_reverse(buffer, inode_data, TIMESTAMP_SIZE*2, 12);
    for(int j = 0; j < TIMESTAMP_SIZE; j++){
        virtual_disk_sos.inodes[id_inode].ctimestamp[j] = buffer[j];
    }
    for(int j = 0; j < TIMESTAMP_SIZE; j++){
        virtual_disk_sos.inodes[id_inode].mtimestamp[j] = buffer[j + TIMESTAMP_SIZE];
    }
    
    virtual_disk_sos.inodes[id_inode].nblock = block_to_uint(&inode_data[25]);
    virtual_disk_sos.inodes[id_inode].first_byte = block_to_uint(&inode_data[26]);
    return res;
}
int write_inode(int id_inode){
    block_t inode_data[INODE_SIZE];
    claire_chan(virtual_disk_sos.inodes[id_inode].filename, inode_data, FILENAME_MAX_SIZE, 0);
    inode_data[8] = *uint_to_block(virtual_disk_sos.inodes[id_inode].size);
    inode_data[9] = *uint_to_block(virtual_disk_sos.inodes[id_inode].uid);
    inode_data[10] = *uint_to_block(virtual_disk_sos.inodes[id_inode].uright);
    inode_data[11] = *uint_to_block(virtual_disk_sos.inodes[id_inode].oright);
    uchar  buffer[TIMESTAMP_SIZE*2];
    ustrcat(buffer, virtual_disk_sos.inodes[id_inode].ctimestamp, virtual_disk_sos.inodes[id_inode].mtimestamp, TIMESTAMP_SIZE, TIMESTAMP_SIZE);
    claire_chan(buffer,inode_data, TIMESTAMP_SIZE*2, 12 );
    inode_data[25] = *uint_to_block(virtual_disk_sos.inodes[id_inode].nblock);
    inode_data[26] = *uint_to_block(virtual_disk_sos.inodes[id_inode].first_byte);
    return write_data(inode_data, 27, INODES_START+id_inode*27);
}

/* ------------------------------------------ */

/* écris la tables d'inode sur le disque */
int write_inodes_table(){
    for(int i = 0; i < INODE_TABLE_SIZE; i++){
        if(write_inode(i) == -1 )
            return -1;
    }
    return 0;

}

int init_inode_table(){
    for(int i = 0; i < INODE_TABLE_SIZE; i++){
        init_inode(i);
    }
    return write_inodes_table();
}
/* ------------------------------------------ */

// supprime l'inode a l'indice i
void delete_inode(int i){
    init_inode(i);
    compacte_inode_table();
    first_free_bytes();

}

void init_inode(int id_inode){
    virtual_disk_sos.inodes[id_inode].filename[0]='\0';
    virtual_disk_sos.inodes[id_inode].size=0;
    virtual_disk_sos.inodes[id_inode].uid=0;
    virtual_disk_sos.inodes[id_inode].uright=0;
    virtual_disk_sos.inodes[id_inode].oright=0;
    virtual_disk_sos.inodes[id_inode].ctimestamp[0]='\0';
    virtual_disk_sos.inodes[id_inode].mtimestamp[0]='\0';
    virtual_disk_sos.inodes[id_inode].nblock=0;
    virtual_disk_sos.inodes[id_inode].first_byte=0;
}

/* ------------------------------------------ */

/* retourne l'index de la premiere inodes inutilisé -1 sinon */ 
int get_unused_inode(){
    // parcours tout les inodes et regarde laquelle a un fichier de taille 0
    for(int i=0;i<INODE_TABLE_SIZE;i++){
        if(strcmp(virtual_disk_sos.inodes[i].filename, "\0") == 0){
            return i;
        }
    }
    return -1;
}

/* ------------------------------------------ */

void up_inode(int id_inode, int new_size){
    virtual_disk_sos.inodes[id_inode].size = new_size;
    virtual_disk_sos.inodes[id_inode].nblock = compute_nblock(new_size);
    strcpy(virtual_disk_sos.inodes[id_inode].mtimestamp, timestamp());
}
/* premet de set une inode avec son nom de fichier et le fichier contenue*/ 
void set_inode(int id_inode,char* s_nom_fichier,int i_taille_fich){
    // et on l'initialise
    strcpy(virtual_disk_sos.inodes[id_inode].filename,s_nom_fichier);
    virtual_disk_sos.inodes[id_inode].size=i_taille_fich;
    virtual_disk_sos.inodes[id_inode].first_byte=virtual_disk_sos.super_block.first_free_byte;
    virtual_disk_sos.inodes[id_inode].nblock = compute_nblock(i_taille_fich);
    strcpy(virtual_disk_sos.inodes[id_inode].ctimestamp,timestamp());
    strcpy(virtual_disk_sos.inodes[id_inode].mtimestamp, virtual_disk_sos.inodes[id_inode].ctimestamp);
    write_inodes_table();
    first_free_bytes();
}

/* ------------------------------------------ */

/* premet d'initialisé une inode avec son nom de fichier a la premiere inodes disponible renvois l'id de l'inode et -1 si il n'y a pas d'inode disponible*/ 
int add_inode(char* s_nom_fichier,int i_taille_fich){
    //recupere la première inode dispo
    int id_inode = get_unused_inode();
    //renvois -1 si aucune inode dispo
    if(id_inode == -1)return -1;
    //l'initialise sinon
    set_inode(id_inode, s_nom_fichier,i_taille_fich);
    //renvois l'id de l'inode créer 
    return id_inode;
    
}

/* ------------------------------------------ */

void cmd_dump_inode(){

}

/* ------------------------------------------ */

/* compacte la table d'inode sur la gauche 
decale tout les éléments a gauche */
void compacte_inode_table(){
    //initialise un iterateur descendant
    int i = INODE_TABLE_SIZE;
    //initialise une balise du dernière inode
    int last_inode = -1;
    while( i >= 0 ){
        
        //set le last_inode sur l'inode avec le plus grand indice
        if(strcmp(virtual_disk_sos.inodes[i].filename, "\0") != 0 && last_inode == -1)last_inode = i;
        //si un trou est trouver décale toutes les inode après ce trou de 1 vers l'avant, 
        //supprime le dernier inode et avance la balise de fin
        if(last_inode != -1 && strcmp(virtual_disk_sos.inodes[i].filename, "\0") == 0){
            for(int j = i; j < last_inode-1; j++){
                virtual_disk_sos.inodes[j] = virtual_disk_sos.inodes[j+1];
            }
            init_inode(last_inode);
            last_inode --;  
        }
        i--;
    }
    //met la table d'inode a jour 
    write_inodes_table(virtual_disk_sos.inodes);
}

void ustrcat(uchar * buffer, uchar * prem, uchar * sec, int prem_size, int sec_size){
    int i = 0, j = 0;
    for(i; i<prem_size ;i++){
        buffer[i] = prem[i];
    }
    for(j; j<sec_size ;j++){
        buffer[i+j] = sec[j];
        //printf("%d\n",j+i);
    }
//    printf(" i %d; j %d; i+j %d\n",i,j, i+j);
    
}


int eteindre_2(){
    write_super_block();
    write_inodes_table();
    return eteindre_1();
}


char *timestamp(){
    time_t current_time;
    char* c_time_string;

    /* Obtain current time. */
    current_time = time(NULL);

    if (current_time == ((time_t)-1))
    {
        (void) fprintf(stderr, "Failure to obtain the current time.\n");
        exit(EXIT_FAILURE);
    }

    /* Convert to local time format. */
    c_time_string = ctime(&current_time);

    if (c_time_string == NULL)
    {
        (void) fprintf(stderr, "Failure to convert the current time.\n");
        exit(EXIT_FAILURE);
    }
    c_time_string[strlen(c_time_string)-1] = '\0';
    return c_time_string;
}