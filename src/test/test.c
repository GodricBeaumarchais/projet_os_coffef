#include "../global.h"
#include "../couche_4/couche4.h"
#include "./test.h"

extern virtual_disk_t virtual_disk_sos;


int main(int argc, char*argv[]){
    printf("DEMARAGE DES TEST:\n\n");
    test_couche_1();
    test_couche_2();
    test_couche_3();
    test_couche_4();
}


void test_couche_1(){
    printf("    Test couche 1 : \n\n");
    test_allumage_extinction();
    test_comput_block();
    test_converstion_block_uint();
    test_lecure_ecriture();
    convertion_ucharstring_blocklist();
    printf("    Test couche 1 validé\n\n");
    eteindre_1();
}

void test_allumage_extinction(){
    printf("        Test allumage/extinction :\n");
    remove("repTest/d0");
    assert(init_disk_sos_1("repTest")==1);printf("            Allumage en mode création du fichier réussi\n");
    assert(eteindre_1() == 0);printf("            exteinction réussi \n");
    assert(init_disk_sos_1("repTest")==0);printf("            Allumage en mode récupération du fichier réussi \n\n");
    
}

void test_comput_block(){
    printf("        Test compute_nblock :\n");
    assert(compute_nblock(0)==0);
    assert(compute_nblock(1)==1);
    assert(compute_nblock(2)==1);
    assert(compute_nblock(3)==1);
    assert(compute_nblock(4)==1);
    assert(compute_nblock(5)==2);
    printf("            Test valide\n\n");
    
}

void test_converstion_block_uint(){
    printf("        Test converstion block/uint uint/block :\n");
    uint a  = 0;
    uint b = 4294967295;
    uint c = 3425654575;
    assert(block_to_uint(uint_to_block(a))==a);
    assert(block_to_uint(uint_to_block(b))==b);
    assert(block_to_uint(uint_to_block(c))==c);
    printf("            Test valide\n\n");
    
}

void test_lecure_ecriture(){
        printf("        Test lecture/ecriture :\n");

    remove("repTest/d0");
    assert(init_disk_sos_1("repTest")==1);printf("            Allumage en mode création du fichier réussi\n");
    block_t * block = malloc(sizeof(block_t));
    block->data[0] = 1;
    block->data[1] = 2;
    block->data[2] = 3;
    block->data[3] = 4;
    block_t * recupTest = malloc(sizeof(block_t));
    assert(write_block_out_of_disk(block,0, virtual_disk_sos.storage)== 0);printf("            Ecriture d'un block dans fichier réussi\n");
    assert(read_block_out_of_disk(recupTest,0, virtual_disk_sos.storage)== 0);printf("            Lecture d'un block dans fichier réussi\n");
    for(int i = 0; i < BLOCK_SIZE; i++){
        assert(block->data[i] == recupTest->data[i]);printf("            Données correctement lus et écrite %d/4\n", i);
    }
    block_t * block2 = malloc(sizeof(block_t));
    block2->data[0] = 5;
    block2->data[1] = 6;
    block2->data[2] = 7;
    block2->data[3] = 8;
    block_t * block3 = malloc(sizeof(block_t));
    block3->data[0] = 9;
    block3->data[1] = 10;
    block3->data[2] = 11;
    block3->data[3] = 255;
    block_t * blockList = malloc(3*sizeof(block_t));
    blockList[0] = *block;
    blockList[1] = *block2;
    blockList[2] = *block3;
    block_t * blockListTest = malloc(3*sizeof(block_t));
    assert(write_data_out_of_disk(blockList,3,0, virtual_disk_sos.storage)== 0);printf("            Ecriture d'une chaine de block dans fichier réussi\n");
    assert(read_data_out_of_disk(blockListTest,3,0, virtual_disk_sos.storage)== 0);printf("            Lecture d'une chaine de block dans fichier réussi\n");
    for(int j = 0; j < 3; j++){
        for(int i = 0; i < BLOCK_SIZE; i++){
            assert(blockList[j].data[i] == blockListTest[j].data[i]);printf("            Données correctement lus et écrite %d/4\n", i);
        }
    }
    free(block);
    free(block2);
    free(block3);
    free(blockList);
    free(blockListTest);
    free(recupTest);
        printf("            Test valide\n\n");

}

void convertion_ucharstring_blocklist(){
        printf("        Test convertion string uchar /block :\n");

    uchar * chaine = malloc(10*sizeof(uchar));
    uchar * chaineTest = malloc(10*sizeof(uchar));
    chaine[0] = 'c';
    chaine[1] = 'o';
    chaine[2] = 'f';
    chaine[3] = 'f';
    chaine[4] = 'e';
    chaine[5] = 'e';
    chaine[6] = 'f';
    chaine[7] = 'u';
    chaine[8] = 'c';
    chaine[9] = 'k';

    block_t * data = malloc(3* sizeof(block_t));

    claire_chan(chaine, data, 10, 0);
    claire_chan_reverse(chaineTest, data,10,0);
    for(int i = 0; i < 10; i++){
        assert(chaine[i] == chaineTest[i]);printf("            Test chaine valide %d/10\n", i+1);
    }
    printf("            Test valide\n\n");


}

/////////////////////////////////////////////
//        couche 2
/////////////////////////////////////////////

void test_couche_2(){
    printf("    Test couche 2 : \n\n");
    remove("repTest/d0");
    assert(init_disk_sos_1("repTest")==1);
    test_init_write_read_sb();
    test_init_write_read_it();
    test_add_set_get_delet_compact_inode();
    assert(eteindre_2() == 0);
    printf("    Test couche 2 validé\n\n");
    
}



void test_init_write_read_sb(){
    
    printf("        Test initialisation/ecriture/lecture superblock :\n");
    assert(init_super_block()== 0);
    printf("            Initialisation/ecriture superblock réussi\n");
    super_block_t * s_b = &virtual_disk_sos.super_block;
    s_b->number_of_files = 9;
    s_b->number_of_users = 6;
    s_b->nb_blocks_used = DISK_SIZE*7;
    s_b->first_free_byte = DISK_SIZE*1;
    assert(read_super_block() == 0);
    printf("            Lecture du superblock réussi\n");
    assert(
        s_b->number_of_files == 0 &&
        s_b->number_of_users == 1 &&
        s_b->nb_blocks_used == DISK_SIZE &&
        s_b->first_free_byte == (int)(DISK_SIZE)*4 
    );printf("          données bien lus et écrite sur le disque");
    printf("        Test valide\n\n");
}

void test_init_write_read_it(){
    printf("        Test initialisation/ecriture/lecture Inode table :\n");
    assert(init_inode_table()== 0);
    printf("            Initialisation/ecriture superblock réussi\n");
    for(int i = 0; i < INODE_TABLE_SIZE; i++){
        virtual_disk_sos.inodes[i].filename[0] = 'u';
        virtual_disk_sos.inodes[i].size = 8;
        virtual_disk_sos.inodes[i].first_byte= 3;
        virtual_disk_sos.inodes[i].ctimestamp[0] = 'u';
        virtual_disk_sos.inodes[i].mtimestamp[0] = 'u';

        }
    assert(read_inodes_table() == 0);
    printf("            Lecture de la table d'inode réussi\n");
    for(int i = 0; i < INODE_TABLE_SIZE; i++){
        assert(virtual_disk_sos.inodes[i].filename[0] == '\0' );
        assert(virtual_disk_sos.inodes[i].size == 0 );
        assert(virtual_disk_sos.inodes[i].first_byte == 0 );
        assert(virtual_disk_sos.inodes[i].ctimestamp[0] == '\0' );
        assert(virtual_disk_sos.inodes[i].mtimestamp[0] == '\0' );
        printf("               récuperation des données %d/%d réussi\n",i,INODE_TABLE_SIZE);
    }
    printf("            récuperation des données complète et correcte\n");
}   

void test_add_set_get_delet_compact_inode(){
    init_super_block();
    printf("        Test initialisation/ecriture/lecture Inode table :\n");
    assert(init_inode_table()== 0);
    printf("            Initialisation/ecriture superblock réussi\n");
    assert(get_unused_inode()==0);
    printf("            Test unised inode 1/4\n");
    set_inode(&virtual_disk_sos.inodes[3], "test1", 8);
    assert(virtual_disk_sos.inodes[3].size == 8);
    assert(virtual_disk_sos.super_block.first_free_byte == 3632);
    printf("            Test set inode 1/1\n");
    assert(get_unused_inode()==0);
    printf("            Test unised inode 2/4\n");
    add_inode("test2", 9);
    assert(virtual_disk_sos.super_block.first_free_byte == 5680);
    assert(virtual_disk_sos.inodes[0].size == 9);
    printf("            Add inode réussi 1/2\n");
    assert(get_unused_inode()==1);
    printf("            Test unised inode 3/4\n");
    add_inode("test3", 10);
    assert(virtual_disk_sos.super_block.first_free_byte == 7728);
    assert(virtual_disk_sos.inodes[1].size == 10);
    printf("            Add inode réussi 2/2\n");
    assert(get_unused_inode()==2);
    printf("            Test unised inode 4/4\n");
    compacte_inode_table();
    assert(get_unused_inode()==3);
    printf("            Test compact table 1/1\n");
    

    
}

///////////////////////////////////////////////
//      couche 3
////////////////////////////////////////////////

void test_couche_3(){
    printf("    Test couche 3 : \n\n");
    test_init_sys3();
    printf("    Test couche 3 validé\n\n");
    
}

void test_init_sys3(){
    printf("        Test init sys\n");
    remove("repTest/d0");
    assert(init_disk_sos_3("repTest") == 1);
    assert(session_usid() == 0);
    printf("            init_disk 1/2  init users table, write user table \n");
   // printf("nam :%s\n", virtual_disk_sos.users_table[3].login);
    for(int i = 0; i < NB_USERS; i++ ){
        strcpy(virtual_disk_sos.users_table[i].login, "uwu ^^ uwu ^^");
    }
    assert(read_user_table() == 0);
    printf("            Lecture des données réussi\n");
    //printf("%s\n", virtual_disk_sos.users_table[3].login);
    assert(strcmp(virtual_disk_sos.users_table[0].login, "ROOT") == 0);
    for(int i = 1; i < NB_USERS; i++ ){
        assert(strcmp(virtual_disk_sos.users_table[i].login, "missing_usr")==0);
    }
    printf("            Recuperation des données réussi\n");
    assert(eteindre_3()==0);
}


void test_couche_4(){
    printf("    Test couche 4 : \n\n");
    remove("repTest/d0");
    init_disk_sos_3("repTest");
    
    uchar * data = "test";

    file_t * file = malloc(sizeof(file_t));
    //printf("%s\n",file->data);
 //   printf("fin init\n");
    strcpy(file->data, data);
   // printf("test1\n");
    file->size = strlen(data);
    //    printf("%s %s\n",file->data, data);

    char name[FILENAME_MAX_SIZE] = "test";
    creat_file(name,file);
    strcpy(file->data, "\0");
    file->size = strlen(data);
    assert(read_file(name,file)==0  );
    //printf("%s %s\n",file->data, data);
    assert(strcmp(file->data, data) == 0
    && file->size == strlen(data));
    
    printf("    Test couche 4 validé\n\n");
    free(file);
}