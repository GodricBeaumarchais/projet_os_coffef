#ifndef _COUCHE4_H
#define _COUCHE4_H

#include "../global.h"
#include "../couche_3/couche3.h"

extern virtual_disk_t virtual_disk_sos;


int creat_file(char name[FILENAME_MAX_SIZE], file_t* file);

int edit_file(char name[FILENAME_MAX_SIZE], file_t* file);


int read_file(char name[FILENAME_MAX_SIZE], file_t * file);


int delete_file(char name[FILENAME_MAX_SIZE]);

int write_file(inode_t* inode, file_t * file);

int search_inode_with_name(char name[FILENAME_MAX_SIZE]);

int chow(char name[FILENAME_MAX_SIZE], char name_user[FILENAME_MAX_SIZE]);

int chmod_4(char name[FILENAME_MAX_SIZE], char groupe, char* droit);
#endif