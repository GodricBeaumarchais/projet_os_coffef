#include "./couche4.h"




int creat_file(char name[FILENAME_MAX_SIZE], file_t* file){
    int id_inode;
    if(search_inode_with_name(name) != -1)return -3;
    else if(( id_inode = add_inode(name, file->size)) == -1)return -2;
    set_droit_creation(id_inode);
    if(write_file(&virtual_disk_sos.inodes[id_inode], file) == 0)return id_inode ;
    return -1;
}

int edit_file(char name[FILENAME_MAX_SIZE], file_t* file){
    int id_inode;
    if(id_inode = search_inode_with_name(name) == -1)return -3;
    if(droit_ecriture(id_inode) >= 0)
        up_inode(id_inode,file->size);
    else return -2;
    if(write_file(&virtual_disk_sos.inodes[id_inode], file) == 0)return id_inode ;
    return -1;
}

int read_file(char name[FILENAME_MAX_SIZE], file_t * file){
    block_t block[FILE_SIZE];
    int id_inode ;
    if(( id_inode = search_inode_with_name(name)) == -1)return -3;
    if(droit_lecture(id_inode) == -1) return -2;
    int res = read_data(block, FILE_SIZE, virtual_disk_sos.inodes[id_inode].first_byte);
    //printf("reed\n");file->size = block_to_uint(&block[0]);
    claire_chan_reverse(file->data, block, MAX_FILE_SIZE, 0);
    file->size = virtual_disk_sos.inodes[id_inode].size;
    //printf("%s read\n", file->data);
    return res;
 
}

int delete_file(char name[FILENAME_MAX_SIZE]){
    int id_inode ;
    if(( id_inode = search_inode_with_name(name)) == -1)return -3;
    if(droit_ecriture(id_inode) == -1)return -2;
    delete_inode(id_inode);
    return 0;
}


int write_file(inode_t* inode, file_t * file){
    block_t block[FILE_SIZE];
    claire_chan(file->data, block, MAX_FILE_SIZE,0);
    return write_data(block, MAX_FILE_SIZE, inode->first_byte);
    
} 


int search_inode_with_name(char name[FILENAME_MAX_SIZE]){
    int i = 0;
    while(strcmp(name,virtual_disk_sos.inodes[i].filename) && i < INODE_TABLE_SIZE)i++;
    if(i == INODE_TABLE_SIZE)return -1;
    return i;

}

int chow(char name[FILENAME_MAX_SIZE], char name_user[FILENAME_MAX_SIZE]){
    int id_inode = search_inode_with_name(name);
    if (id_inode == -1)
        return -3;
    else{
        change_uid(id_inode, name_user);
    }
}

int chmod_4(char name[FILENAME_MAX_SIZE], char groupe, char* droit){
    int id_inode = search_inode_with_name(name);
    if (id_inode == -1)
        return -3;
    else{
        chmod_3(id_inode, groupe, droit);
    }
}
