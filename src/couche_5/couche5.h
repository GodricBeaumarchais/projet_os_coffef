#ifndef _COUCHE5_H
#define _COUCHE5_H
#include "../global.h"
#include "../couche_4/couche4.h"


typedef struct st_commande{
    char** ligne_commande;
    int i_nb_token;
}Commande;

/**
 * @brief peremt l'affichage de l'entête de la ligne de commandes
 * 
 * @param sz_login le login de l'utilisateur
 */
void printEntete(char* sz_login);

/**
 * @brief permet l'affichage de la liste de tous les fichiers au format détaillé (-l) ou non (nom du fichier et sa taille)
 * 
 * @param commande structure contenant la ligne de commande et le nombre de token de la commande
 */
void commandeLs(Commande* commande);

/**
 * @brief recherche l'existence d'un fichier dans la tables d'inodes
 * 
 * @param fichier nom du fichier recherché
 * 
 * @return renvoie la position de l'inode du fichier dans la table d'inodes
 */
int fichierExiste(char* fichier);

/**
 * @brief permet l'affichage d'un fichier
 * 
 * @param commande structure contenant la ligne de commande qui elle même contient le nom du fichier à afficher
 */
void commandeCat(Commande* commande);

void lireFichier(file_t * file);
/**
 * @brief permet la suppression d'un fichier
 * 
 * @param commande ligne de commande contenant le nom du fichier à supprimer en premier argument
 */
void commandeRm(Commande* commande);

/**
 * @brief permet la création d'un fichier
 * 
 * @param commande ligne de commande contenant le nom du fichier à créer en premier argument
 */
void commandeCr(Commande* commande);

/**
 * @brief permet la modificaton du contenu d'un fichier par le contenu tapé au clavier
 * 
 * @param commande ligne de commande qui contient le nom du fichier à modifier en premier argument
 */
void commandeEdit(Commande* commande);

/**
 * @brief permet de copier un fichier du système hôte sur le système d'exploitation
 * 
 * @param commande ligne de commande contenant le nom du fichier à copier en premier argument
 */
void commandeLoad(Commande* commande);

/**
 * @brief permet de copier un fichier du système d'exploitation sur le système hôte
 * 
 * @param commande ligne de commande contenant le nom du fichier à copier en premier argument
 */
void commandeStore(Commande* commande);

/**
 * @brief permet de changer le propriétaire d'un fichier si le demandeur en a les droits
 * 
 * @param commande ligne de commande contenant en premier argument le nom du fichier et en deuxieme argument le login du nouveau propiétaire
 */
void commandeChown(Commande* commande);

/**
 * @brief permet de modifier les droits des autres utilisateurs sur un fichier si le demandeur en a les droits
 * 
 * @param commande ligne de commande contenant en premier paramètre le nom du fichier et en deuxième les nouveaux droits
 */
void commandeChmod(Commande* commande);

/**
 * @brief permet l'affichage des différents utilisateurs
 */
void commandeListUsers();

/**
 * @brief permet l'ajout d'un utilisateur par le root, demande le login et le mot de passe du nouvel utilisateur
 *
 * @param commande contenant le nombre de token de la ligne de commande qui doit être a 1
 */
void commandeAdduser(Commande* commande);

/**
 * @brief permet la suppression d'un utilisateur par le root
 * 
 * @param commande ligne de commande contenant en premier argument le login de l'utilisateur à supprimer
 */
void commandeRmuser(Commande* commande);

/**
 * @brief permet l'appel des différentes commandes
 * 
 * @param commande ligne de commande à exécuter qui a pour premier argument le nom de la commande
 * @param sz_login login de l'utilisateur courant pour les commandes root
 */
void executer_commande(Commande* commande);

/**
 * @brief permet la libération de la strucutre commande
 * 
 * @param commande structure commande a libérer
 */
void free_mem(Commande* commande);

/**
 * @brief permet le déroulement de l'interprétation de la commande : lecture analyse exécution
 * 
 * @param sz_login login de l'utilisateur courant pour l'affichage de l'entête
 */
void interpreteur_commande();

/**
 * @brief réalise la connexion d'un utilisateur en vérifiant son mot de passe et son login
 */
void connection();


#endif 