#ifndef _COUCHE4_H
#define _COUCHE4_H

/** 
 * @brief sauvegarde le systeme de fichier sur le disque (super block, table inodes)
 */
void sauvegardeSysFichier();

/**
 * @brief calcul du hash et verification du mdp et du login
 * 
 * @param login login de l'utilisateur
 * @param mdp mot de passe de l'utilisateur
 *
 * @return true si le mdp et le login correspondent false sinon
 */
bool log_usr(char* login,char* mdp);

/**
 * @brief retourne le contenue du fichier si l'utilisateur a les droits NULL sinon
 * 
 * @param login le login de l'utilisateur 
 * @param nom_fichier le nom du fichier que l'ont veux lire
 * 
 * @return le contenu du fichier ou NULL
 */
char* lireFichier(char* nom_fichier);

/**
 * @brief supprime un fichier en verifiant si l'utilisateur a les droits
 * 
 * @param login le login de l'utilisateur
 * @param nom_fichier le nom du fichier a supprimer
 * 
 * @return un boolean true si le fichier a été supprimer false si l'utilisateur n'a pas les droits
 */
bool supprimerFichier(char* nom_fichier);

/** 
 * @brief crée un fichier sur le disque dur
 * 
 * @param login le login de l'utilisateur
 * @param nom_fichier nom du fichier a créer
 * 
 * @return retourne true si le fichier a été créer false sinon (disque plein)
 */
bool creeFichier(char* nom_fichier);

/**
 * @brief ecris le contenue de nv_contenue_fichier dans le nom_fichier qui est a la position premier_octet_fichier si l'utilisateur a les droits
 * 
 * @param premier_octet_fichier int premier octet du fichier
 * @param nv_contenu_fihier contenu a ecrire dans le fichier
 * @param login le login de l'utilisateur
 * @param nom_fichier nom du fichier a editer
 * 
 * @return true si le fichier a ete modifier false sinon (utilisateur n'a pas les droits)
 */
bool writeInFichier(int premier_octet_fichier,char* nv_contenu_fichier,char* nom_fichier);

/**
 * @brief verifie si l'utilisateur a le droit de modifier le propriétaire du fichier nom_fichier qui a pour inode l'inode a index_inode
 * 
 * @param login login de l'utilisateur qui veux changer le proiétaire du fichier
 * @param nom_fichier nom du fichier dont on veux changer le propiétaire
 * @param index_inode index de l'inode du fichier
 * @param nv_login login du nouveau propriétaire du fichier
 * 
 * @return retourne true si le propriétaire du fichier a été modifié et false si il n'avait pas les droits
 */
bool updateProprioFile(char* nom_fichier,int index_inode,char* nv_login);

/** 
 * @brief change les droit droits d'un fichier nom_fichier pour tout les autres utilisateur si il en possèdes les droits
 * 
 * @param login login de l'utilisateur qui fais la demande de changement de droit pour les autres
 * @param nom_fichier nom du fichier dont on veux changer les droits
 * @param droits nouveau droit du fichier
 * 
 * @return true si le changement a été effectuer false sinon 
 */
bool changerDroitFich(char* nom_fichier,int droits);

/**
 * @brief ajoute l'utilisateur avec nv_login et nv_mdp si c'est possible l'utilisateur login est le root (pas besoin de le revérifié)
 * 
 * @param nv_login login du nouvel utilisateur
 * @param nv_mdp mdp du nouvel utilisateur
 * @param login root
 * 
 * @return true si l'utilisateur a été crée false sinon 
 */
bool addUser(char* nv_login,char* nv_mdp);

/** 
 * @brief supprime le loginSuppr
 * 
 * @param login root PRECONDITION 
 * @param login_suppr login a supprimer
 * 
 * @return true si le login a été supr false sinon
 */
bool rmUser(char* login_suppr);

/**
 * @brief rempli le tableau list_user par des string contenant chacun un user
 * 
 * @param list_user tableau de string contenant les users
 * 
 * @return retourne le nombre de user
 */
int listUser(char* list_user[NB_USERS]);
#endif