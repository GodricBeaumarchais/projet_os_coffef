
#include "couche5.h"

extern virtual_disk_t virtual_disk_sos;

void printEntete(char* sz_login){
    printf("%s-ScratchOS:~/",sz_login);
}

void commandeLs(Commande* commande){
    // si il y a que deux argument et que le deuxieme est bien -l 
    inode_t * inode = virtual_disk_sos.inodes;
    if(commande->i_nb_token == 2 && strcmp(commande->ligne_commande[1],"-l") == 0 ){
        // alors on parcours les inodes
        for(int i=0;i<INODE_TABLE_SIZE;i++){
            // on regarde si l'inode existe
            if(strcmp(inode[i].filename, "\0") != 0){
                // et on affiche le details de l'inode
                printf("\nName : %s, size : %d,user id : %d, user right : %d, other right : %d, date creation : %s, date derniere modif : %s, nombre blocks : %d, first bytes : %d\n"
                ,inode[i].filename,inode[i].size,inode[i].uid,inode[i].uright,inode[i].oright,
                inode[i].ctimestamp,inode[i].mtimestamp,inode[i].nblock,inode[i].first_byte);
            }
        }
        
    }
    else if(commande->i_nb_token == 2 && strcmp(commande->ligne_commande[1],"-a") == 0 ){
        for(int i=0;i<INODE_TABLE_SIZE;i++){
                // on affiche que le nom et la taille
                printf("Name : %s, Size : %d\n",inode[i].filename,inode[i].size);
            
        }
    }
    // sinon si il y a qu'un seul argument
    else if(commande->i_nb_token == 1){
        for(int i=0;i<INODE_TABLE_SIZE;i++){
            if(inode[i].filename[0] != '\0'){
                // on affiche que le nom et la taille
                printf("Name : %s, Size : %d\n",inode[i].filename,inode[i].size);
            }
        }
    }
    else{
        // sinon il y a une erreur sur l'argument
        printf("Erreur sur l'argument\n");
    }
}

int fichierExiste(char* fichier){
    for(int i=0;i<INODE_TABLE_SIZE;i++){
        if(strcmp(virtual_disk_sos.inodes[i].filename,fichier) == 0){
            return i;
        }
    }
    return -1;
}

void commandeCat(Commande* commande){
    // si le nombre de token n'est pas de deux il y a forcement une erreurs
    int index_inode=-1;
    if(commande->i_nb_token != 2){
        printf("Erreur sur l'argument\n");
    }

    file_t * file = malloc(sizeof(file_t));
    int res= read_file(commande->ligne_commande[1], file);
    

    // si la lecture a retourner NULL alors l'utilisateur n'a pas les droits
    if(res < -1){
       printf("L'utilisateur n'a pas les droits\n");
    }
    else if (res == 0){
        // sinon on affiche le contenue du fichier
        lireFichier(file);
    }
    else{
        printf("Erreur sur le nom du fichier\n");
    }
    free(file);
}  

void lireFichier(file_t * file){
    printf("\n");
    for(int i = 0; i < file->size; i++){
        printf("%c",file->data[i]);
    }
    printf("\n");
}

void commandeRm(Commande* commande){
    bool fichier_supprimer=false;
    // verification du nombre d'argument
    if(commande->i_nb_token != 2){
        printf("Erreur sur l'argument\n");
    }
    int res = delete_file(commande->ligne_commande[1]);
    //verification si le fichier existe
    
    if(res == -3)
     printf("Erreur sur le nom du fichier\n");
    else if(res == -2)
        printf("L'utilisateur n'a pas les droits\n");
    else
        printf("Le fichier %s supprimer\n",commande->ligne_commande[1]);
    
}

void commandeCr(Commande* commande){
    int creation_fichier;
    char buffer_data[MAX_FILE_SIZE];
    // verification du nombre d'argument
    if(commande->i_nb_token != 2){
        printf("Erreur sur l'argument\n");
    }
    else{
        printf("Veuillez entré le contenue du fichier\n");
        // creation du fichier
        fgets(buffer_data,MAX_FILE_SIZE,stdin);
        file_t * file = malloc(sizeof(file_t));
        strcpy(file->data, buffer_data);
        file->size = strlen(file->data);

        creation_fichier=creat_file(commande->ligne_commande[1], file);

        // on informe l'utilisateur
        if(creation_fichier == -1){
            delete_file(commande->ligne_commande[1]);
            printf("Erreur ecriture\n");
        }
        else if(creation_fichier== -2)
            printf("Le disque dur est saturé\n");
        else if( creation_fichier == -3)
            printf("Le fichier existe déjà\n");
        else{
            printf("Le fichier %s a bien été crée\n",commande->ligne_commande[1]);
        }
        free(file);
    }
    
}

void commandeEdit(Commande* commande){
    int edit_fichier;
    char sz_contenu_fichier[MAX_FILE_SIZE];

    // verification des arguments
    if(commande->i_nb_token!=2){
        printf("Erreur sur l'argument\n");
    }
    fgets(sz_contenu_fichier,MAX_FILE_SIZE,stdin);
    file_t * file = malloc(sizeof(file_t));
    strcpy(file->data, sz_contenu_fichier);
    file->size = strlen(sz_contenu_fichier);
    int res = edit_file(commande->ligne_commande[1], file);
    if(res == -3)
            printf("Erreur sur le nom du fichier\n");
    else if(res == -2)
            printf("L'utilisateur n'a pas les droits d'éditions\n");
    else if(res == -1)
        printf("Erreur ecriture\n");
    else
            printf("Le fichier %s a bien été edité\n",commande->ligne_commande[1]);


}

void commandeLoad(Commande* commande){
    // verification des arguments
    if(commande->i_nb_token!=2){
        printf("Erreur sur l'argument\n");
    }
    FILE * file = fopen(commande->ligne_commande[1],"r");
    if(file == NULL){
        printf("Erreur nom du fichier\n");
        return;
    }
    uchar buffer[MAX_FILE_SIZE];
    fread(buffer, 1, MAX_FILE_SIZE, file);
    file_t * file_sys = malloc(sizeof(file_t));
    strcpy(file_sys->data, buffer);
    file_sys->size = strlen(buffer);
    int i = strlen(commande->ligne_commande[1])-1;
    while(commande->ligne_commande[1][i] != '/')i--;
    char name[FILENAME_MAX_SIZE];
    int j = -1;
    while(commande->ligne_commande[1][i] !='\0' && j < FILENAME_MAX_SIZE){
        i++;
        j++;
        name[j] = commande->ligne_commande[1][i];
    }
    creat_file(name, file_sys);

}

void commandeStore(Commande* commande){
    // verification des arguments
    if(commande->i_nb_token!=2){
        printf("Erreur sur l'argument\n");
    }
    else{
        file_t * file_sys = malloc(sizeof(file_t));
        int res = (read_file( commande->ligne_commande[1],file_sys) != 0);
            if(res < -1){
               printf("L'utilisateur n'a pas les droits\n");
               return;
            }
            else if (res == -1)
            printf("Erreur sur le nom du fichier\n");
               return;
        
        FILE * file = fopen(commande->ligne_commande[1],"w");
        if(file == NULL){
            printf("Erreur fichier existe\n");
            return;
        }
        fwrite(file_sys->data, 1, file_sys->size, file);

    }
    // chercher sur le systeme un fichier et copie son contenue et le systeme "hote" (ordi)
}

void commandeChown(Commande* commande){
    int index_inode=-1;
    // verification du nombre d'argument
    if(commande->i_nb_token!=3){
        printf("Erreur sur les arguments\n");
    }
     // on regarde si le fichier existe si oui on recupere l'index de son inode
    int res = chow(commande->ligne_commande[1],commande->ligne_commande[2]);
    if(res == -3)
                printf("Erreur sur le nom du fichier\n");
    else if(res == -2)
            printf("L'utilisateur n'a pas les droits de changements\n");
    else if (res == -4)
        printf("L'utilisateur entré n'esxiste pas\n");
    else
                printf("le propriétaire du fichier %s a bien été changé en %s\n",commande->ligne_commande[1],commande->ligne_commande[2]);

    

}

void commandeChmod(Commande* commande){

    int index_inode=0;
    bool statu_exec=false;
    // verification du nombre de parametre
    if(commande->i_nb_token!=3){
        printf("Erreur sur les arguments\n");
    }
    // on regarde si le fichier existe si oui on recupere l'index de son inode
    int res = chow(commande->ligne_commande[1],commande->ligne_commande[2] );
    
    if(res == -1)
        printf("Erreur ecriture\n");
    else if(res == -2)
        printf("L'utilisateur n'a pas les droits de changements\n");
    else if(res == -3)
        printf("Erreur sur le nom du fichier\n");
    else if(res == -4)
        printf("Erreur arg 1\n");
    else if(res == -5)
        printf("Erreur arg 2\n");
}

void commandeListUsers(){
    char* list_users[NB_USERS];
    int nb_user=0;

    // recuperation de la liste des utilisateur
    for(int i=0;i<nb_user;i++){
        // affichage du nom des utilisateurs
        printf("%s ",virtual_disk_sos.users_table[i].login);
    }
    printf("\n");
}

void commandeAdduser(Commande* commande){
    // declaration des variables
    char sz_nv_login[FILENAME_MAX_SIZE];
    char sz_nv_mdp[FILENAME_MAX_SIZE];
    bool b_ajout_utilisateur=false;

    // verification du nombre de parametre
    if(commande->i_nb_token != 1){
        printf("Erreur sur la commande\n");
    }
    else{
        // on demande le nouveau login
        printf("Login : ");
        scanf("%s",sz_nv_login);
        // on demande le nouveau mot de passe
        printf("Mot de passe : ");
        scanf("%s",sz_nv_mdp);
        
        // on ajoute l'utilisateur
        b_ajout_utilisateur=add_user(sz_nv_login,sz_nv_mdp);

        // on regarde si l'utilisateur a bien été ajouté ou si il y a eu une erreur
        if(b_ajout_utilisateur){
            printf("L'utilisateur %s a bien été ajouté\n",sz_nv_login);
        }
        else{
            printf("Impossible d'ajouter un utilisateur\n");
        }
    }
}

void commandeRmuser(Commande* commande){
    // verification du nombre de parametre
    if(commande->i_nb_token != 1){
        printf("Erreur sur la commande\n");
    }
    else{
        int res = remove_user(commande->ligne_commande[1]);
        if(res == -1)
            printf("Erreur ecriture\n");
        else if(res == -2)
            printf("L'utilisateur n'a pas les droits\n");
        else if(res == -3)
            printf("Erreur sur le nom utilisateur\n");
       
    }
}

void executer_commande(Commande* commande){
    if(strcmp(commande->ligne_commande[0],"quit") == 0){
        eteindre_3();
        exit(0);
    }
    else if(strcmp(commande->ligne_commande[0],"ls") == 0){
        commandeLs(commande);
    }
    else if(strcmp(commande->ligne_commande[0],"cat") == 0){
        commandeCat(commande);
    }
    else if(strcmp(commande->ligne_commande[0],"rm") == 0){
        commandeRm(commande);
    }
    else if(strcmp(commande->ligne_commande[0],"cr") == 0){
        commandeCr(commande);
    }
    else if(strcmp(commande->ligne_commande[0],"edit") == 0){
        commandeEdit(commande);
    }
    else if(strcmp(commande->ligne_commande[0],"load") == 0){
        commandeLoad(commande);
    }
    else if(strcmp(commande->ligne_commande[0],"store") == 0){
        commandeStore(commande);
    }
    else if(strcmp(commande->ligne_commande[0],"chown") == 0){
        commandeChown(commande);
    }
    else if(strcmp(commande->ligne_commande[0],"chmod") == 0){
        commandeChmod(commande);
    }
    else if(strcmp(commande->ligne_commande[0],"listusers") == 0){
        commandeListUsers();
    }
    else if(strcmp(commande->ligne_commande[0],"adduser") == 0){
        commandeAdduser(commande);
    }
    else if(strcmp(commande->ligne_commande[0],"rmuser") == 0){
        commandeRmuser(commande);
    }
    else{
        printf("Erreur sur la commande\n");
    }
}

void free_mem(Commande* commande){
    for(int i =0;i<commande->i_nb_token;i++){
        // on parcours tout les mot et on les libere
        free(commande->ligne_commande[i]);
    }
    // on libere le tableau 
    free(commande->ligne_commande);
    commande->i_nb_token=0;
}

void interpreteur_commande(){
    // declaration des variables
    bool b_fin_commande=false;
    //buffer de commande de talle 100 char
    char sz_buffer_commande[FILENAME_MAX_SIZE*3+4];
    Commande commande;
    int indexInBuffer;
    // nombre de mot token dans la commande
    commande.i_nb_token=0;

    while(true){
        // on malloc la tableau de string
        commande.ligne_commande=malloc(sizeof(char*));
        // entete de ligne
        printEntete(login_current_user());
        // lecture de la ligne
        fgets(sz_buffer_commande,FILENAME_MAX_SIZE*3+4,stdin);
        indexInBuffer=0;
        // on reinitilialise le fais qu'on a lus la ligne de commande
        b_fin_commande=false;
        while(!b_fin_commande){

            // on ajoute une case dans le tableau de string 
            commande.i_nb_token++;
            commande.ligne_commande=realloc(commande.ligne_commande,sizeof(char*)*commande.i_nb_token);
            // on malloc ce string de la taille max d'un fichier
            commande.ligne_commande[commande.i_nb_token-1]=malloc(sizeof(char)*FILENAME_MAX_SIZE);

            // on lit caractère par caractère tant qu'on a pas atteint la fin d'un mot ou la fin de la ligne
            commande.ligne_commande[commande.i_nb_token-1][0]='\0';
            while(sz_buffer_commande[indexInBuffer] != ' ' && sz_buffer_commande[indexInBuffer] != '\0' && sz_buffer_commande[indexInBuffer] != '\n'){
                // concatene un carectere au string
                strncat(commande.ligne_commande[commande.i_nb_token-1],&sz_buffer_commande[indexInBuffer],1);
                indexInBuffer++;
            }
            indexInBuffer++;

            // si on a atteint la fin de la ligne on execute la commande et on termine
            if(sz_buffer_commande[indexInBuffer] == '\0'){
                executer_commande(&commande);
                b_fin_commande=true;
            }
        }
        free_mem(&commande);
    }
}

void connection(){
    char sz_login[FILENAME_MAX_SIZE];
    char sz_motdepasse[60];
    bool verif_valide=false;
    while(!verif_valide){
        // on demande le login
        printf("login (32 char max): ");
        scanf("%s",sz_login);

        // on demande le mot de passe
        printf("Mot de passe (60 char max) : ");
        scanf("%s",sz_motdepasse);

        // on vide le buffer d'entrer
        rewind(stdin);

        // si le login existe
        if(log_usr(sz_login,sz_motdepasse) != -1){
            interpreteur_commande();
        }
        else{
            printf("Erreur sur la saisit\n");
        }

    }
}

int main(void){
    printf("Veuillez entré le nom du repertoire disk ( moins de 256 char )\n");
    char name[256];
    scanf("%s", name);
    int res = init_disk_sos_3(name) ;
    if(res == 1)
        printf("disque créer\n");
    else if(res == 0)
        printf("données du disque récupéré\n");
    else 
        printf("erreur initialisation disque\n");

    connection();
    return 0;
}