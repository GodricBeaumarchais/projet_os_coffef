/**
 * @file couche1.h
 * @brief couche regroupant les fonctions de la couche 1 du système (la lecture et l'écriture disque ainsi que le démarrage/extinction système).
 * @version 0.1
 * @date 2022-05-04
 * 
 * @copyright Copyright(c) 2022
 * 
 */
#ifndef _COUCHE1_H
#define _COUCHE1_H
#include "../global.h"



/**
 * @brief initialise les disques virtuels disque sos (storage), la table d'inode et le superblock le sont couche 2 et la table utilisateur couche 3
 * 
 * @param [in] r_name nom du répertoir où stocker les données
 *
 * @return 0 si le fichier existe et vient d'être ouvert/ 1 si le fichier vient d'être créé et -1 en cas d'erreur
 */
int init_disk_sos_1(char* r_name);

/**
 * @brief convertit une chaîne de caractères en block ;
 * 
 * @param chaine chaîne à convertir
 * @param data liste de block dans laquelle stocker les données 
 * @param size taille de la chaîne 
 */
void claire_chan( uchar * chaine, block_t * data,int size,int pos);

void claire_chan_reverse( uchar * chaine, block_t * data,int size, int pos );

/**
 * @brief Récupère la séquence binaire d'un uchar dans buffer
 * 
 * @param data 
 * @param buffer 
 */
void uchar_to_bin(uchar data, char buffer[BYTE]);


/**
 * @brief Retourne la valeur d'un uint dans un block
 * 
 * @param x entier à convertir 
 * @return block_t
 */
 block_t * uint_to_block(unsigned int x);

/**
 * @brief convertit un block en uint
 * 
 * @param block 
 * @return unsigned int 
 */
uint block_to_uint(block_t *block);

/**
 * @brief Retourne le nombre de block nécessaires pour stocker un nombre n d'octets.
 * 
 * @param [in] n_octets nombre d'octets pour lesquels calculer 
 * @return nombre de blocks
 */
int compute_nblock(int n_octets);

/**
 * @brief Ecrit un block block à la position pos sur le disque système. Retourne -1 en cas d'erreur.
 * 
 * @param [in] block block à écrire
 * @param [in] pos position sur laquelle écrire
 * @return code d'erreur
 */
int write_block(block_t *block,int pos);

/**
 * @brief Lit un block block à la position pos sur le disque systeme. Retourne -1 en cas d'erreur.
 * 
 * @param [out] block block dans lequel stocker le résulat
 * @param [in] pos position sur laquelle trouver le block à lire
 * @return 0 si tout se passe bien, -1 en cas d'erreur
 */
int read_block(block_t *block,int pos);

/**
 * @brief Ecrit une donnée data à la position pos sur le disque système. Retourne -1 en cas d'erreur.
 * @param [in] data liste de block a stocker
 * @param [in] nb_block  nombre de blocs a lire
 * @param [in] pos position a laquel lire les blocs
 * @return 0 si tout se passe bien, -1 en cas d'erreur
 */
int write_data(block_t * data, int nb_block , int pos);

/**
 * @brief Lit une donnée data à la position pos sur le disque systeme. Retourne -1 en cas d'erreur.
 * @param [out] data liste de blocks à stocker
 * @param [in] nb_block nombre de blocks à lire
 * @param [in] pos position sur laquelle lire les blocks
 * @return 0 si tout se passe bien, -1 en cas d'erreur
 */
int read_data(block_t * data, int nb_block, int pos) ;

/**
 * @brief Ecrit un block block sur la position pos sur un fichier spécifié. Retourne -1 en cas d'erreur.
 * 
 * @param [in] block block à écrire
 * @param [in] pos position sur laquelle écrire
 * @param [in] file fichier dans lequel écrire la data
 * @return code d'erreur
 */
int write_block_out_of_disk(block_t *block,int pos, FILE* file);

/**
 * @brief Lit un block block à la position pos sur un fichier spécifié. Retourne -1 en cas d'erreur.
 * 
 * @param [out] block block dans lequel stocker le résulat
 * @param [in] pos position sur laquelle trouver le bloc à lire
 * @param [in] file fichier dans lequel lire la data
 * @return 0 si tout se passe bien, -1 en cas d'erreur
 */
int read_block_out_of_disk(block_t *block,int pos, FILE* file) ;

/**
 * @brief Ecrit un block block à la position pos sur un fichier spécifié. Retourne -1 en cas d'erreur.
 * 
 * @param [in] data blocks à écrire
 * @param [in] nb_block nombre de blocks à lire
 * @param [in] pos position sur laquelle écrire
 * @param [in] file fichier dans lequel écrire la data
 * @return code d'erreur
 */
int write_data_out_of_disk(block_t * data, int nb_block, int pos, FILE* file);

/**
 * @brief Lit un block block à la position pos sur un fichier spécifié. Retourne -1 en cas d'erreur.
 * 
 * @param [out] data block dans lesquels stocker le résultat
 * @param [in] nb_block nombre de blocks à lire
 * @param [in] pos position sur laquelle trouver le bloc à lire
 * @param [in] file fichier dans lequel lire la data
 * @return 0 si tout se passe bien, -1 en cas d'erreur
 */
int read_data_out_of_disk(block_t *data, int nb_block, int pos, FILE* file);

/**
 * @brief extinction du système au niveau 1 
 *  
 */
int eteindre_1();


#endif