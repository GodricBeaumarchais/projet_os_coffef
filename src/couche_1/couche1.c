#include "./couche1.h"


virtual_disk_t virtual_disk_sos;


int init_disk_sos_1(char* r_name) {
    
    /* Initialisation de FILE *storage : */    
    //creation du sous pass vers le fichier
    char temp[128];
    strcpy(temp, "mkdir ");
    strcat(temp,r_name);
    system(temp);    char *f_name = malloc(strlen(r_name) + 3 + 1);
    strcpy(f_name,r_name);
    strcat(f_name, "/d0");
    printf("%s\n", f_name);
    //tentative d'ouverture du fihier
    virtual_disk_sos.storage = fopen(f_name,"r+b");
    //si il n'existe pas creation du fichier
    if(virtual_disk_sos.storage == NULL){
        virtual_disk_sos.storage = fopen(f_name,"w+b");
        //verification de creation et return -1 en cas d'erreur
        if(virtual_disk_sos.storage == NULL){
            free(f_name);
            return -1;
        }
        free(f_name);
        return 1;
    }
    //sinon lecture des inodes/user/superblock dans le fichier
    else{
        return 0;
    }
    
}

//string_uchar_to_block_chaine
void claire_chan( uchar * chaine, block_t * data,int size, int pos ){
    int p = 0;
    for(int  i = 0; i < compute_nblock(size); i++){

        for(int j = 0; j< BLOCK_SIZE; j++){
            if(i*4+j < size)
                data[pos + i].data[j] = chaine[i*4+j];
            else
                data[pos + i].data[j] = 0;
        }
    }
}

//block_chain_to_string_uchar
void claire_chan_reverse( uchar * chaine, block_t * data,int size, int pos ){
    for(int  i = 0; i < compute_nblock(size); i++){
        for(int j = 0; j< BLOCK_SIZE; j++){
            if(i*4+j < size)
                chaine[i*4+j] = data[pos + i].data[j];
        }
    }
}

void uchar_to_bin(uchar data, char buffer[BYTE]) {
    for (int i = 7; i >= 0; i--) {
        if ((int)data >= pow(2,i)) {
            buffer[i] = '1';
            data -= pow(2,i);
        } else buffer[i] = '0';
    }
}


uint block_to_uint(block_t *block) {
    uint res = 0;
    char bin_seq[BYTE]; //sequence binaire d'un uchar du tableau block->data
    int p = BLOCK_SIZE*BYTE-1; //puissance 
    for (int i = 0; i < BLOCK_SIZE; i++) {
        uchar_to_bin(block->data[i],bin_seq); //on récupère la séquence binaire
        for (int j = BYTE-1; j >= 0 ; j--) {
            if (bin_seq[j] == '1'){
            	res += pow(2,p);
            }
            p--;
        }
    }
    return res;
}

 block_t * uint_to_block(uint x){
	block_t * block = malloc(sizeof(block_t));
	for(int i = 0; i < BLOCK_SIZE; i++){
		block->data[i] = (int)(x/pow(2,BYTE*(BLOCK_SIZE-i-1)));
		x = x%(int)(pow(2,BYTE*(BLOCK_SIZE-i-1)));
	}
	return block;
 }

int compute_nblock(int nb_octets){
    int nb_block = nb_octets/BLOCK_SIZE;
    if(nb_octets%BLOCK_SIZE)nb_block++;
    return nb_block;
}
/* int delete_block(int pos) {
    block_t *block = malloc(sizeof(block_t));
 
    fseek(virtual_disk_sos.storage, BLOCK_SIZE*pos, SEEK_SET);
    for (int i = 0; i < BLOCK_SIZE; i++)
        block->data[i] = 0;
    if (write_block(block,pos) == 0)
        return 0;
    return -1; //code d'erreur
}
 */
bool block_cmp(block_t * fblock, block_t * sblock,int size){
    for(int  i = 0; i < size; i++){
        for(int j = 0; j< BLOCK_SIZE; j++){
            if(fblock[i].data[j] != sblock[i].data[j])
                return false;
        }
    }
    return true;
}



int write_block(block_t *block,int pos) {
    return write_block_out_of_disk(block, pos, virtual_disk_sos.storage);
    
}

int read_block(block_t *block,int pos) {
    return read_block_out_of_disk(block, pos, virtual_disk_sos.storage);
}



int write_data(block_t * data, int nb_block, int pos) {
    return write_data_out_of_disk(data, nb_block, pos, virtual_disk_sos.storage);
}

int read_data(block_t *data, int nb_block, int pos) {
    return read_data_out_of_disk(data, nb_block, pos, virtual_disk_sos.storage);
}



int write_block_out_of_disk(block_t *block,int pos, FILE* file){
    fseek(file,(BLOCK_SIZE)*pos,SEEK_SET);
    for(int i = 0; i < BLOCK_SIZE; i++){
      //  printf("    data %d ecrit\n", i);
        if ((int) fwrite(&block->data[i],1,1, file) == -1) 
            return -1;
    }
    return 0;
}


int read_block_out_of_disk(block_t *block,int pos, FILE* file){
    fseek(file,(BLOCK_SIZE)*pos,SEEK_SET);
    for(int i = 0; i < BLOCK_SIZE; i++){
        if ((int)fread(&block->data[i],1,1, file) == -1)
            return -1;
    }
    return 0;
}


int write_data_out_of_disk(block_t * data, int nb_block, int pos, FILE* file){
    //printf("preseek : %ld\n",file);
    fseek(file,(BLOCK_SIZE)*pos,SEEK_SET);
    //printf("postseek\n");    
    for(int i = 0; i < nb_block; i++){
        
    //  printf("block %d ecrit\n", i);
        if (write_block_out_of_disk(&data[i],pos+i,file) == -1)
            return -1;
    }
    return 0;
}


int read_data_out_of_disk(block_t *data, int nb_block, int pos, FILE* file){
    fseek(file,(BLOCK_SIZE)*pos,SEEK_SET);
    for(int i = 0; i < nb_block; i++){
        if (read_block_out_of_disk(&data[i],pos+i,file) == -1)
            return -1;
    }
    return 0;
}


int eteindre_1(){
    return fclose(virtual_disk_sos.storage);
}


