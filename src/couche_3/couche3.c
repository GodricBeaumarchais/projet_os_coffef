#include "couche3.h"

extern virtual_disk_t virtual_disk_sos;

static session_t session; 

int init_disk_sos_3(char * r_name){
    session.userid = 0;
    
    int res = init_disk_sos_2(r_name);
    if(res == 0){
        if(read_user_table() == -1)return -1;
        return 0;
    }
    else if( res == 1 ){

        if(init_user_table() == -1)return -1;
        return 1;
    }
    return -1;
}

int add_user(char login[FILENAME_MAX_SIZE] , char* password){
    //recup la table usilisateur
    users_table_t * users_table = &virtual_disk_sos.users_table;
    //vérifie les droits pour créer un utilisateur 
    if(droit_valide(0)){
        //cherche un utilisateur libre
        int new_usrId = search_missing_usr(users_table);
        if( ! strcmp(login, ROOT_NAME)
        && ! strcmp(login, MISSING_USR)
        ){
               ustrcat( users_table[new_usrId]->login,users_table[new_usrId]->login, login, 0, FILENAME_MAX_SIZE);
                crypt_password(users_table[new_usrId]->passwd, password); 
            }
        return new_usrId;
        }
        return -1;
    }


int log_usr( char login[FILENAME_MAX_SIZE] , char* mdp){
    user_t * users_table = virtual_disk_sos.users_table;
    int i = 0;
    char cryp_mdp[SHA256_BLOCK_SIZE*2 + 1];
    crypt_password(cryp_mdp, mdp);
    //recherche un login correspondant
    while(strcmp(login, users_table[i].login) == 0
             &&  i<NB_USERS )i++;
    //si il ne le trouve pas renvois -1
    if(i == NB_USERS)return -1;
    //revois l'usrId trouvé si le mdp correspond
    else if (strcmp(users_table[i].passwd, cryp_mdp) == 0  ){
        session.userid = i;
        return i;
        }
    //renvois -1 sinon
    return -2;
}


void crypt_password(char* crypted_psw, char*password){
    sha256ofString((uchar *)password,crypted_psw);   
}

int search_missing_usr(){
    users_table_t* users_table = &virtual_disk_sos.users_table;
    int i = 1;
    while(strcmp(users_table[i]->login,MISSING_USR) != 0 && i < NB_USERS)i++;
    if(i == 5)return -1;
    return i;
}

int search_user(char name[FILENAME_MAX_SIZE]){
    users_table_t* users_table = &virtual_disk_sos.users_table;
    int i = 1;
    while(strcmp(users_table[i]->login,name) != 0 && i < NB_USERS)i++;
    if(i == 5)return -1;
    return i;
}

bool droit_valide( uint num_usr){
    return session.userid == num_usr || session.userid == 0;
}

 int init_user_table(){
    user_t * users_table = virtual_disk_sos.users_table;
    //printf("%ld\n", sizeof(users_table[ROOT_UID]->login));
    //initialise le login et le mdp du root
    strcpy(users_table[ROOT_UID].login, ROOT_NAME); 
    crypt_password(users_table[ROOT_UID].passwd, ROOT_PSW);
    //initialise le reste en missing_usr
    for(int i = 1; i < NB_USERS; i++ ){
        strcpy(users_table[i].login, MISSING_USR); 
    }
    //printf("debut init user\n");
    int res = write_user_table();
    //printf("fin ini user\n");
    return res;
}

int write_user_table(){
    //printf("oui\n");
    user_t * users_table = virtual_disk_sos.users_table;
    uchar buffer[(int)(FILENAME_MAX_SIZE+SHA256_BLOCK_SIZE*2+1)*(int)NB_USERS];
    int buffer_len = 0;
    for(int i = 0; i < NB_USERS; i++){
        ustrcat(buffer,buffer, users_table[i].login, buffer_len, FILENAME_MAX_SIZE);
        buffer_len += FILENAME_MAX_SIZE;
        ustrcat(buffer,buffer, users_table[i].passwd, buffer_len, SHA256_BLOCK_SIZE*2+1);
        buffer_len += SHA256_BLOCK_SIZE*2 + 1;
    }

   //for(int i = 0; i < buffer_len; i++)
        //printf("%d %c\n",i,buffer[i]);
    
    
    block_t block[USER_TABLE_SIZE];
    
    claire_chan(buffer, block, buffer_len,0);
    

    int res =  write_data(block, USER_TABLE_SIZE, USER_START);    
        printf("fin write user\n");
    return res; 
}

int read_user_table(){
    user_t * users_table = virtual_disk_sos.users_table;
    block_t block[USER_TABLE_SIZE];
    int res = read_data(block, USER_TABLE_SIZE, USER_START);
    
uchar buffer[(int)(FILENAME_MAX_SIZE+SHA256_BLOCK_SIZE*2+1)*NB_USERS];
    int buffer_len = (int)(FILENAME_MAX_SIZE+SHA256_BLOCK_SIZE*2+1)*NB_USERS;
    claire_chan_reverse(buffer, block, buffer_len, 0);
    int current = 0 ;
    for(int i = 0; i < NB_USERS; i++){
        for(int j = 0; j< FILENAME_MAX_SIZE; j++){
            users_table[i].login[j] = buffer[current];
            current ++;
        }
        for(int k = 0; k < SHA256_BLOCK_SIZE*2+1; k++){
            users_table[i].passwd[k] = buffer[current];
            current ++;
        }
    }
    return res;
}


void set_droit_creation( int id_inode){
    virtual_disk_sos.inodes[id_inode].uid = session.userid;
    virtual_disk_sos.inodes[id_inode].uright = 3;
    virtual_disk_sos.inodes[id_inode].oright = 0;
    write_inodes_table();
}

bool modifie_droit(int id_inode, uint droit){
    if(session.userid != virtual_disk_sos.inodes[id_inode].uid ||session.userid == 0)return false;
    virtual_disk_sos.inodes[id_inode].oright = droit;
    return true;
}

int session_usid(){
    return session.userid;
}

int eteindre_3(){
    write_user_table();
    return eteindre_2();
}

int droit_ecriture(int id_inode){
    if(droit_valide(virtual_disk_sos.inodes[id_inode].uid)){
        if(virtual_disk_sos.inodes[id_inode].uright%2 == 1)return 0;
        else return -1;
    }
    else if(virtual_disk_sos.inodes[id_inode].oright%2 == 1)return 1;
    else return -1;
       
}

int chmod_3(int id_inode, char groupe, char* droit){
    uint * right;
    if(!droit_valide(virtual_disk_sos.inodes[id_inode].uid))return -2;
    if(groupe == 'o'){
        right = &virtual_disk_sos.inodes[id_inode].oright;
    }
    else if(groupe == 'u'){
        right = &virtual_disk_sos.inodes[id_inode].uright;
    }
    else
        return -5;
    right = 0;
    if(droit[0] == 'R')
        right += 2;
    else if (droit[0] != 'r')
        return -4;
    if(droit[0] == 'W')
        right += 1;
    else if (droit[0] != 'w')
        return -4;
    return write_inodes_table();
    


}

int droit_lecture(int id_inode){
    if(droit_valide(virtual_disk_sos.inodes[id_inode].uid)){
        if(virtual_disk_sos.inodes[id_inode].uright>=2 == 1)return 0;
        else return -1;
    }
    else if(virtual_disk_sos.inodes[id_inode].oright>=2 == 1)return 1;
    else return -1;
       
}

int change_uid(int id_inode, char name[FILENAME_MAX_SIZE]){
    int uid = search_user(name);
    if(uid = -1){
        return -4;
    }
    if(!droit_valide(virtual_disk_sos.inodes[id_inode].uid))
        return -2;
    virtual_disk_sos.inodes[id_inode].uid = uid;
    return write_inodes_table();
}

char * login_current_user(){
    return virtual_disk_sos.users_table[session.userid].login;
}


int remove_user(char name[FILENAME_MAX_SIZE]){
    int usrid = search_user(name);
    if(!droit_valide(0))return -2;
    if(usrid == -1)return -3;
    else{
        strcpy(virtual_disk_sos.users_table[usrid].login, MISSING_USR);
        for(int i = 0; i < INODE_TABLE_SIZE; i++){
            if(virtual_disk_sos.inodes[i].uid == usrid)
                return change_uid(i,"ROOT");
        }
    }

}