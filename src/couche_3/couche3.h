/**
 * @file couche3.h
 * @brief couche regroupant les fonctions de la couche 3 du system s'occupant de toute la gestion des utilisateurs.
 * @version 0.1
 * @date 2022-05-04
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef _COUCHE3_H
#define _COUCHE3_H
#include "../global.h"
#include "../couche_2/couche2.h"    

/**
 * @brief initialise le système au niveau 3
 * 
 * @param r_name nom du répertoire
 * @return int  valeur -1 si erreur, 0 sinon
 */
int init_disk_sos_3(char * r_name);

/**
 * @brief ajoute un tilisateur à la table
 * 
 * @param [in] login pseudo de l'utilisateur à créer
 * @param [in] password mot de passe de l'utilisateur à créer
 * @return int id de l'utilisateur créé
 */
int add_user(char login[FILENAME_MAX_SIZE], char* password);

/**
 * @brief permet de se connecter à la session d'un utilisateur
 * 
 * @param [in] login pseudo entré par l'utilisateur
 * @param [in] mdp mot de passe entré par l'utilisateur
 * @return int l'id de l'utilisateur ou -1 en cas de pseudo incorrect ou -2 en cas de mot de passe incorrect
 */
int log_usr( char login[FILENAME_MAX_SIZE], char* mdp);

/**
 * @brief permet d'initialiser un utilisateur comme inexistant
 * 
 * @param [out] user utilisateur à réinitialiser
 */
void reset_user(user_t* user);


/**
 * @brief permet de crypter un mot de passe
 * 
 * @param [out] crypted_psw sortie du mot de passe crypté
 * @param [in] password mot de passe à crypter
 */
void crypt_password(char* crypted_psw, char*password);

/**
 * @brief cherche le premier utilisateur non assigné
 * 
 * @return int renvoie l'id utilisateur correspondant ou -1 s'ils existent tous
 */
int search_missing_usr();

/**
 * @brief vérifie si l'utilisateur connecté correspond à celui placé en param
 * 
 * @param num_usr 
 * @return true utilisateur correspondantvirtual_disk_sos.inodes[id_inode].
 * @return false utilisateur non correspondant
 */
bool droit_valide( uint num_usr);


/**
 * @brief initialise la table d'utilisateur et avant ça appelle (init_inode_table->init_disk_sos)
 * 
 */
int init_user_table();

/**
 * @brief écrit la table utilisateurs sur le disque
 * 
 * @return int  valeur -1 si erreur, 0 sinon
 */
int write_user_table();

/**
 * @brief lit la table des utilisateurs sur le disque
 * 
 * @return int  valeur -1 si erreur, 0 sinon
 */
int read_user_table();

/**
 * @brief initialise les droits à la création d'un fichier
 * 
 * @param inode pointeur vers l'inode correspondant au fichier
 */
void set_droit_creation( int id_inode);

/**
 * @brief modifie les droits d'un fichier
 * 
 * @param id_inode inode correspondant au fichier
 * @param droit droits à affecter
 * @return true permission de modifier les droits accordés
 * @return false permission de modifier les droits non accordés
 */
bool modifie_droit(int id_inode, uint droit);

/**
 * @brief renvoie l'uid de la session courante 
 * 
 * @return int uid de la session courante 
 */
int session_usid();

/**
 * @brief éteint le système au niveau 3
 * 
 * @return int  valeur -1 si erreur, 0 sinon
 */
int eteindre_3();

int droit_ecriture(int id_inode);

int droit_lecture(int id_inode);

int change_uid(int id_inode, char name[FILENAME_MAX_SIZE]);

int chmod_3(int id_inode, char groupe, char* droit);

int remove_user(char name[FILENAME_MAX_SIZE]);

char * login_current_user();
#endif
