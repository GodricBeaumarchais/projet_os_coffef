CC=gcc
EXEC=out
CFLAGS=-W
LDFLAGS=-lm
SRC=  ./src/couche_5/couche5.c ./src/couche_1/couche1.c ./src/couche_2/couche2.c ./src/couche_3/couche3.c ./src/Sha256/sha256_utils.c ./src/Sha256/sha256.c ./src/couche_4/couche4.c 
OBJ= $(SRC: .c=.o)

EXECTEST=testOut
SRCTEST= ./src/test/test.c ./src/couche_1/couche1.c ./src/couche_2/couche2.c ./src/couche_3/couche3.c ./src/Sha256/sha256_utils.c ./src/Sha256/sha256.c ./src/couche_4/couche4.c 
OBJTEST= $(SRCTEST: .c=.o)


ifeq ($(DEBUG),yes)
	CFLAGS += -g
else
	CFLAGS += -O3 -DNDEBUG
endif

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c %.h
	@$(CC) -o $@ -c $< $(CFLAGS)

test: $(EXECTEST)

$(EXECTEST): $(OBJTEST)
	$(CC) -o $@ $^ $(LDFLAGS)

%test.o: %.c %.h
	@$(CC) -o $@ -c $< $(CFLAGS)


clean:
	find . -name "*.o" -type f -delete
	find . -name "out" -type f -delete
	find . -name "testOut" -type f -delete

doc: ./src/Doc/Doxyfile
	doxygen ./src/Doc/Doxyfile
	rm ./index.html
	ln -s ./src/Doc/html/index.html ./
	

test:

./src/couche_3/couche3.c: ./src/Sha256/sha256_utils.h ./src/Sha256/sha256.h
