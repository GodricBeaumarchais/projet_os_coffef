package main;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Window extends JFrame implements ActionListener {
    public Window() {
        super("OOF");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(200,300);
        this.setLocationRelativeTo(null);
        //ContentPane
        JPanel content_pane = (JPanel) this.getContentPane();
        content_pane.setLayout(new GridLayout(0,1));
        //Button
        JButton defragmentation_button = new JButton("Defragmentation du disque");
        JButton analyse_button = new JButton("Analyse");
        content_pane.add(defragmentation_button);
        content_pane.add(analyse_button);
        //Ajout d'un actionListener pour détecter les clics sur les boutons
        defragmentation_button.addActionListener(this);
        analyse_button.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Defragmentation du disque")) {
            System.out.println("Défragmentation du disque en cours...");
        
            JOptionPane.showMessageDialog(this, "Défragmentation du disque terminé.");
        } else if (e.getActionCommand().equals("Analyse")) {
            System.out.println("Analyse du système en cours...");

            JOptionPane.showMessageDialog(this, "Analyse système terminé.");
        }
    }
}