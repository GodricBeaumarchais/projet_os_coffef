    Normalement tout est fonctionel sauf doute sur la couche 1

 make : pour compiler et créer un exécutable out 

make test : pour compiler les tests et créer un exécutable testOut

make doc : pour générer la doc et créer un index.html pour y accéder

make clean : pour supprimer tous les dossiers générés 

lien du git : https://gitlab.com/GodricBeaumarchais/projet_os_coffef.git

après avoir lancé l'exécutable veuillez spécifier le nom du répertoire du disque 
et pour une première connexion veuillez vous connecter avec les identifiants ROOT et admin 

voici la liste des commandes :
ls utilisation : 
ls [-l]
liste le contenu du catalogue sans argument (nom fichier, taille) avec (tout)

cat utilisation :
cat <nom fichier>
Effectue l'affichage du contenu du fichier a l'écran

rm utilisation : 
rm <nom fichier>
Effectue la suppression du fichier

cr utilisation :
cr <nom fichier>
Effectue la création du fichier dont le nom est passé en paramètre

edit utilisation :
edit <nom fichier>
Effectue la modification du contenu du fichier par ce qui est tapé au clavier par l'utilisateur

load utilisation :
load <nom fichier>
Effectue la copie d'un fichier situé sur le système hote sur le système courant avec le même nom

store utilisation :
store <nom fichier> 
Effectue la copie d'un fichier situé sur le système courant sur le système hote avec le même nom  

chown utilisation :
chown <nom fichier> <nom utilisateur> 
Effectue le changement de propriétaire d'un fichier avec le nom d'utilisateur passé en paramètre

chmod utilisation :
chmod <nom fichier> <groupe> (droit prut prendre les valeurs o et u) <droit> (droit peu prendre les valeurs [rw rW Rw RW])
Effectue le changement de droit pour tout le groupe précisé

listusers utilisation :
listusers
Affiche la liste des utilisateurs

quit utilisation : 
quit 
Quitte l'interpréteur de commande

Commande root:

adduser utilisation :
adduser
Permet de crée un nouvelle utilisateur

rmuser utilisation :
rmuser <nom utilisateur> 
Supprime un l'utilisateur